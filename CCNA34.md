# Configuring 2811 router (RIPv2 and OSPF)

The fallowing sections explains the step to perform to configure 2811 Cisco Routers using routing algorithms such as RIPv2 and OSPF.

# Table of contents
1. [RIPv2](#RIPv2)
2. [OSPF](#OSPF)
3. [Commands](#Commands)
    1. [Hostname](#Hostname)
	2. [Banner](#Banner)
	3. [IPv4](#IPv4)
	4. [RIPv2](#RIPv2)
	5. [Configuring SSH](#Configuring SSH)
	6. [Changing RIPv2 to OSPF](#Changing RIPv2 to OSPF)
	7. [Redistribute (OSPF to RIPv2 and RIPv2 to OSPF)](#Redistribute (OSPF to RIPv2 and RIPv2 to OSPF))
	8. [Configuring Serial Ports](#Configuring Serial Ports)
	9. [DTE or DCE](#DTE or DCE)
	10. [Configuring OSPF for the Serial Ports](#Configuring OSPF for the Serial Ports)
	11. [MD5 Authentication configuration in RIPv2](#MD5 Authentication configuration in RIPv2)
	12. [Removing default config from Switch](#Removing default config from Switch)

## RIPv2
Routing Information Protocol (RIP) is a [Vector Distance](https://fr.wikipedia.org/wiki/Vector_Distance) [IP routing](https://fr.wikipedia.org/wiki/Routage_IP) protocol based on the [Bellman-Ford](https://fr.wikipedia.org/wiki/Bellman-Ford) decentralized route determination algorithm. It allows each router to communicate with neighboring routers. The [metric](https://fr.wikipedia.org/wiki/M%C3%A9trique_(routage)) used is the distance that separates a router from an IP network determined by the number of hops.

For each known IP network, each router keeps the address of the neighboring router with the smallest metric. These best routes are broadcast every 30 seconds.

## OSPF
Open Shortest Path First (OSPF) is a [routing protocol](https://en.wikipedia.org/wiki/Routing_protocol) for Internet Protocol (IP) networks. It uses a [link state routing](https://en.wikipedia.org/wiki/Link-state_routing_protocol) (LSR) algorithm and falls into the group of [interior gateway protocols](https://en.wikipedia.org/wiki/Interior_gateway_protocol) (IGPs), operating within a single [autonomous system](https://en.wikipedia.org/wiki/Autonomous_system_(Internet)) (AS). It is defined as OSPF Version 2 in [RFC 2328](https://tools.ietf.org/html/rfc2328) (1998) for IPv4.

## Commands

### Hostname

Changing the name of the router. Changing from **Router** to **R4**.

	Router>enable
	Router#configure terminal
	Router(config)#hostname R4
	R4(config)#exit

### Banner

Adding a banner to notify users when connecting to the router.
The router will display *Hello ! My name is R4 :)*

	R4#configure terminal
	R4(config)#banner motd $Hello ! My name is R4 :)$
	R4(config)#exit

### IPv4

Giving IPv4 addresses to the router interfaces (**FastEthernet0/0** and **FastEthernet0/1**).

	R4#configure terminal
	R4(config)#interface FastEthernet0/0
	R4(config-if)#ip address 145.1.0.2 255.255.0.0
	R4(config-if)#no shutdown
	R4(config-if)#exit

	R4(config)#interface FastEthernet0/1
	R4(config-if)#ip address 193.4.0.1 255.255.0.0
	R4(config-if)#no shutdown
	R4(config-if)#exit
	R4(config)#exit

> \[:warning: WARNING\] Do not forget `no shutdown` command.

### RIPv2

Adding routing algorithm RIPv2 to the router.

	R4#configure terminal
	R4(config)#router rip
	R4(config-router)#version 2
	R4(config-router)#no auto-summary
	R4(config-router)#network 192.4.0.0
	R4(config-router)#network 145.1.0.0
	R4(config-router)#exit

### Configuring SSH

Adding security layer to the router.
This layer will provide an ssh access to the router, allowing users to connect through ssh.

	R4(config)#enable secret mysecretpassword
	R4(config)#ip domain-name r4.com
	R4(config)#crypto key generate rsa general-keys modulus 1024
	R4(config)#username cisco secret cisco
	R4(config)#line vty 0 15
	R4(config-line)#transport input ssh 
	R4(config-line)#login local
	R4(config-line)#exit
	R4(config)#ip ssh version 2
	R4(config)#exit

> \[:warning: WARNING\] Do not use `cisco` as password. **This is just an exemple**.

### Changing RIPv2 to OSPF

The fallowing commands will change the algorithm on the network 145.1.0.0. Switching from RIPv2 to OSPF.

	R4(config)#router rip
	R4(config-router)#version 2
	R4(config-router)#no network 145.1.0.0
	R4(config-router)#exit
	R4(config)#router ospf 100
	R4(config-router)#network 145.1.0.0 0.0.255.255 area 0
	R4(config-router)#exit

> \[:warning: WARNING\] OSPF and RIPv2 are not compatible to each other. Show the next section to do the conversion.

### Redistribute (OSPF to RIPv2 and RIPv2 to OSPF)

The `redistribute` command will convert OSPF packets to RIPv2 packets and RIPv2 packets to OSPF packets.

	R4#configure terminal
	R4(config)#router rip
	R4(config-router)#redistribute ospf 100 metric 1
	R4(config-router)#exit
	R4(config)#router ospf 100
	R4(config-router)#redistribute rip
	R4(config)#exit

> \[:warning: WARNING\] Be careful to the metrics. Never put **16** for the metric. Change metric for whatever you want.
>> In this exemple web choose 1 instead of 16.

### Configuring Serial Ports

	R4#configure terminal
	R4(config)#interface Serial0/0/0 
	R4(config-if)#ip address 192.168.1.13 255.255.255.252
	R4(config-if)#no shutdown

## DTE or DCE

This command tells you if you are at the `DTE` side or `DCE` side.

	R4#show controllers Serial0/0/0
	R4#show controllers Serial0/0/1

> \[:warning: WARNING\] If the serial port is DCE side, put the clock to **64000**.
>> `R4(config-if)#clock rate 64000`

### Configuring OSPF for the Serial Ports

	R4(config)#router ospf 100
	R4(config-router)#network 192.168.1.0 0.0.0.3 area 0
	R4(config-router)#network 192.168.0.12 0.0.0.3 area 0
	R4(config-router)#exit

### MD5 Authentication configuration in RIPv2

	R4(config)#key chain ka1
	R4(config-keychain)#key 1
	R4(config-keychain-key)#key-string salleA214
	R4(config-keychain-key)#exit
	R4(config-keychain)#exit
	R4(config)#interface FastEthernet 0/1
	R4(config-if)#ip rip authentication mode md5
	R4(config-if)#ip rip authentication key-chain ka1
	R4(config-if)#exit

> \[:paperclip: NOTE\] `salleA214` is the key-word of the authentication.

### Removing default config from Switch

	Switch#erase startup-config
	Switch#delete flash:vlan.dat
	Switch#reload
